package com.skripsi.pointofsale.controller;

import com.skripsi.pointofsale.helper.HelperUtil;
import com.skripsi.pointofsale.jpa.entity.Restaurant;
import com.skripsi.pointofsale.request.order.OrderRequest;
import com.skripsi.pointofsale.request.order.UpdateRequest;
import com.skripsi.pointofsale.service.CartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping("/cart")
@CrossOrigin(origins = "*")
public class CartController {

    @Autowired
    private HelperUtil helperUtil;

    @Autowired
    private CartService cartService;
//    @PostMapping()
//    public ResponseEntity<?> createOrder(@RequestHeader("Authorization") String authorization, @RequestBody OrderRequest order) {
//        log.info("Incoming Request for endpoint /order With Time {} And request {}", LocalDateTime.now());
//        Restaurant restaurant = helperUtil.checkAuth(authorization);
//        return restaurantService.createOrder(restaurant, order);
//    }
//
//    @PatchMapping("/order")
//    public ResponseEntity<?> updateOrder(@RequestHeader("Authorization") String authorization, @RequestBody UpdateRequest order) {
//        log.info("Incoming Request for endpoint /order With Time {} And request {}", LocalDateTime.now());
//        Restaurant restaurant = checkAuth(authorization);
//        return restaurantService.updateOrder(restaurant, order);
//    }
}
