package com.skripsi.pointofsale.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skripsi.pointofsale.exception.ValidationException;
import com.skripsi.pointofsale.jpa.entity.Restaurant;
import com.skripsi.pointofsale.request.customer.UpdateCustomerRequest;
import com.skripsi.pointofsale.request.staff.LoginRequest;
import com.skripsi.pointofsale.request.staff.StaffRegisterRequest;
import com.skripsi.pointofsale.security.JWTGenerator;
import com.skripsi.pointofsale.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping("/customer")
@CrossOrigin(origins = "*")
public class CustomerController {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CustomerService customerService;
    @Autowired
    private JWTGenerator jwtGenerator;

    @PostMapping("/register")
    public ResponseEntity<String> registerCustomer(@RequestHeader("Authorization") String authorization, @Valid @RequestBody StaffRegisterRequest staffRegisterRequest) throws JsonProcessingException {
        log.info("Incoming Request for endpoint /customer/register With Time {} And request {}", LocalDateTime.now(),objectMapper.writeValueAsString(staffRegisterRequest));
        Restaurant restaurant= checkAuth(authorization);
        return customerService.register(staffRegisterRequest,restaurant);
    }

    @PostMapping("/login")
    public ResponseEntity<?> loginStaff(@RequestHeader("Authorization") String authorization,@Valid @RequestBody LoginRequest loginRequest) throws JsonProcessingException {
        log.info("Incoming Request for endpoint /restaurant/login With Time {} And request {}", LocalDateTime.now(),objectMapper.writeValueAsString(loginRequest));
        Restaurant restaurant= checkAuth(authorization);
        return customerService.login(restaurant,loginRequest);
    }

    @GetMapping("/users")
    public ResponseEntity<?> getUsers(@RequestHeader("Authorization") String authorization){
        log.info("Incoming Request for endpoint GET /users With Time {} ", LocalDateTime.now());
        Restaurant restaurant= checkAuth(authorization);
        return customerService.getUsers(restaurant);
    }

    @PostMapping("/users")
    public ResponseEntity<?> updateUsers(@RequestHeader("Authorization") String authorization, @Valid @RequestBody UpdateCustomerRequest updateRequest) {
        log.info("Incoming Request for endpoint POST /users With Time {} ", LocalDateTime.now());
        Restaurant restaurant= checkAuth(authorization);
        return customerService.updateUsers(updateRequest);
    }

    private Restaurant checkAuth(String authorization) {
        Restaurant restaurant = jwtGenerator.checkJWT(authorization);
        if(restaurant==null){
            throw new ValidationException("Invalid Authorization");
        }
        try {
            log.info(objectMapper.writeValueAsString(restaurant));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return restaurant;
    }
}
