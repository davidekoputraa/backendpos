package com.skripsi.pointofsale.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skripsi.pointofsale.jpa.entity.Category;
import com.skripsi.pointofsale.jpa.entity.Menu;
import com.skripsi.pointofsale.jpa.entity.Restaurant;
import com.skripsi.pointofsale.request.menu.MenuRequest;
import com.skripsi.pointofsale.request.menu.StatusMenu;
import com.skripsi.pointofsale.request.order.*;
import com.skripsi.pointofsale.service.MainService;
import com.skripsi.pointofsale.service.RestaurantService;
import com.skripsi.pointofsale.exception.ValidationException;
import com.skripsi.pointofsale.security.JWTGenerator;
import io.minio.errors.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Base64;

@RestController
@Slf4j
@CrossOrigin(origins = "*")
public class MainController {
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private JWTGenerator jwtGenerator;

    @Autowired
    private MainService mainService;

    @GetMapping()
    public ResponseEntity<?> testServer() {
        log.info("Server Connection Success");
        return ResponseEntity.ok().body("Success");
    }

    @GetMapping("/menu")
    public ResponseEntity<?> getMenu(@RequestParam(required = false, name = "category") String category, @RequestHeader("Authorization") String authorization) throws JsonProcessingException {
        log.info("Incoming Request for endpoint get /menu With Time {}", LocalDateTime.now());
        Restaurant restaurant = checkAuth(authorization);
        return restaurantService.getMenu(restaurant, category);
    }

    @GetMapping("/category")
    public ResponseEntity<?> getCategory(@RequestHeader("Authorization") String authorization) throws JsonProcessingException {
        log.info("Incoming Request for endpoint get /menu With Time {}", LocalDateTime.now());
        Restaurant restaurant = checkAuth(authorization);
        return restaurantService.getCategory(restaurant);
    }

    @PostMapping("/category")
    public ResponseEntity<?> addCategory(@RequestHeader("Authorization") String authorization, @RequestBody Category category) throws JsonProcessingException {
        log.info("Incoming Request for endpoint get /menu With Time {}", LocalDateTime.now());
        Restaurant restaurant = checkAuth(authorization);
        return restaurantService.addCategory(restaurant, category.getCategoryName());
    }

    @PostMapping("/menu")
    public ResponseEntity<?> addMenu(@RequestHeader("Authorization") String authorization, @RequestBody MenuRequest menu) throws JsonProcessingException {
        log.info("Incoming Request for endpoint /menu With Time {} And request {}", LocalDateTime.now(), objectMapper.writeValueAsString(menu));
        Restaurant restaurant = checkAuth(authorization);
        return restaurantService.addMenu(menu, restaurant);
    }

    @PostMapping("/menu/status")
    public ResponseEntity<?> changeStatusMenu(@RequestHeader("Authorization") String authorization, @RequestBody StatusMenu menu) throws JsonProcessingException {
        log.info("Incoming Request for endpoint /menu With Time {} And request {}", LocalDateTime.now(), objectMapper.writeValueAsString(menu));
        Restaurant restaurant = checkAuth(authorization);
        return restaurantService.changeStatus(menu, restaurant);
    }

    @GetMapping("/order")
    public ResponseEntity<?> getOrder(@RequestHeader("Authorization") String authorization,
                                      @RequestHeader(required = false, name = "customer_id") String customerId) {
        log.info("Incoming Request for endpoint /Getorder With Time {} And customer_id {}", LocalDateTime.now(),customerId);
        Restaurant restaurant = checkAuth(authorization);
        return restaurantService.getOrder(restaurant, customerId);
    }

    @GetMapping("/order/ongoing")
    public ResponseEntity<?> getOrderOngoing(@RequestHeader("Authorization") String authorization,
                                              @RequestHeader(required = false, name = "customer_id") String customerId) {
        log.info("Incoming Request for endpoint /order With Time {} And request {}", LocalDateTime.now());
        Restaurant restaurant = checkAuth(authorization);
        return restaurantService.getOrderOngoing(restaurant, customerId);
    }

    @GetMapping("/order/finished")
    public ResponseEntity<?> getOrderFinished(@RequestHeader("Authorization") String authorization,
                                      @RequestHeader(required = false, name = "customer_id") String customerId) {
        log.info("Incoming Request for endpoint /order With Time {} And request {}", LocalDateTime.now());
        Restaurant restaurant = checkAuth(authorization);
        return restaurantService.getOrderFinished(restaurant, customerId);
    }

    @PostMapping("/order")
    public ResponseEntity<?> createOrder(@RequestHeader("Authorization") String authorization, @RequestBody OrderRequest order) {
        log.info("Incoming Request for endpoint /order With Time {} And request {}", LocalDateTime.now());
        Restaurant restaurant = checkAuth(authorization);
        return restaurantService.createOrder(restaurant, order);
    }

    @PatchMapping("/order")
    public ResponseEntity<?> updateOrder(@RequestHeader("Authorization") String authorization, @RequestBody UpdateRequest order) {
        log.info("Incoming Request for endpoint /order With Time {} And request {}", LocalDateTime.now());
        Restaurant restaurant = checkAuth(authorization);
        return restaurantService.updateOrder(restaurant, order);
    }

    @PostMapping("/order-detail/status")
    public ResponseEntity<?> orderStatusDetail(@RequestHeader("Authorization") String authorization, @RequestBody UpdateStatusOrderDetailRequest order) {
        log.info("Incoming Request for endpoint /order With Time {} And request {}", LocalDateTime.now());
        Restaurant restaurant = checkAuth(authorization);
        return restaurantService.orderDetailStatus(restaurant, order);
    }

    @PostMapping("/order/status")
    public ResponseEntity<?> orderStatus(@RequestHeader("Authorization") String authorization, @RequestBody UpdateStatusOrderRequest order) {
        log.info("Incoming Request for endpoint /order With Time {} And request {}", LocalDateTime.now());
        Restaurant restaurant = checkAuth(authorization);
        return restaurantService.orderStatus(restaurant, order);
    }

    @PostMapping("/report-order")
    public ResponseEntity<?> reportOrder(@RequestHeader("Authorization") String authorization, @RequestBody ReportOrderRequest order) {
        log.info("Incoming Request for endpoint /order With Time {} And request {}", LocalDateTime.now());
        Restaurant restaurant = checkAuth(authorization);
        return restaurantService.reportOrder(restaurant, order);
    }

    @PostMapping("/upload")
    public ResponseEntity<?> uploadFile(@RequestParam(value = "file") MultipartFile file, @RequestParam(value = "type", required = false) String type, @RequestParam(value = "name") String name, @RequestParam(value = "Authorization") String authorization) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        Restaurant restaurant = checkAuth(authorization);
        return mainService.uploadFile(file, restaurant, type, name);
    }

    @GetMapping("/data")
    public ResponseEntity<?> getData(@RequestHeader("Authorization") String authorization) throws JsonProcessingException {
        log.info("Incoming Request for endpoint /restaurant/login With Time {}", LocalDateTime.now());
        Restaurant restaurant= checkAuth(authorization);
        return restaurantService.getData(restaurant);
    }

    @GetMapping("/list")
    public ResponseEntity<?> listFile() throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        return ResponseEntity.ok(mainService.listFile());
    }

    @GetMapping("/download/{fileName}")
    public ResponseEntity<?> downloadFile(@PathVariable String fileName) {
        return mainService.downloadFile(fileName);
    }

    @DeleteMapping("/delete/{fileName}")
    public ResponseEntity<String> deleteFile(@PathVariable String fileName) {
        return new ResponseEntity<>(mainService.deleteFile(fileName), HttpStatus.OK);
    }

    private Restaurant checkAuth(String authorization) {
        Restaurant restaurant = jwtGenerator.checkJWT(authorization);
        if (restaurant == null) {
            throw new ValidationException("Invalid Authorization");
        }
        return restaurant;
    }
}
