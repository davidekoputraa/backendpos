package com.skripsi.pointofsale.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.skripsi.pointofsale.exception.ValidationException;
import com.skripsi.pointofsale.helper.HelperUtil;
import com.skripsi.pointofsale.jpa.entity.Restaurant;
import com.skripsi.pointofsale.request.PaymentGateway.PayRequest;
import com.skripsi.pointofsale.security.JWTGenerator;
import com.skripsi.pointofsale.service.PaymentGatewayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@Slf4j
@RestController
@CrossOrigin(origins = "*")
public class PaymentGatewayController {

    @Autowired
    PaymentGatewayService paymentGatewayService;

    @Autowired
    private JWTGenerator jwtGenerator;

    @Autowired
    private HelperUtil checkAuth;
    @PostMapping("/pay")
    public ResponseEntity<?> payToPG(@RequestHeader("Authorization") String authorization, @RequestBody PayRequest payRequest) throws JsonProcessingException {
        log.info("Incoming Request for endpoint /pay With Time {}", LocalDateTime.now());
        Restaurant restaurant= checkAuth.checkAuth(authorization);
        return paymentGatewayService.pay(restaurant,payRequest);
    }

    public Restaurant checkAuth(String authorization) {
        Restaurant restaurant = jwtGenerator.checkJWT(authorization);
        if(restaurant==null){
            throw new ValidationException("Invalid Authorization");
        }
        log.info(restaurant.toString());
        return restaurant;
    }
}
