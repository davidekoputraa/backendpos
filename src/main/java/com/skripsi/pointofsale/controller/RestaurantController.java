package com.skripsi.pointofsale.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skripsi.pointofsale.exception.ValidationException;
import com.skripsi.pointofsale.jpa.entity.FullProfile;
import com.skripsi.pointofsale.jpa.entity.Restaurant;
import com.skripsi.pointofsale.request.restaurant.LoginRequest;
import com.skripsi.pointofsale.request.restaurant.RegisterRequest;
import com.skripsi.pointofsale.request.restaurant.Token;
import com.skripsi.pointofsale.service.RestaurantService;
import com.skripsi.pointofsale.response.restaurant.RegisterResponse;
import com.skripsi.pointofsale.security.JWTGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;


@RestController
@Slf4j
@RequestMapping("/restaurant")
@CrossOrigin(origins = "*")
public class RestaurantController {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private JWTGenerator jwtGenerator;

    @PostMapping("/register")
    public ResponseEntity<RegisterResponse> registerRestaurant(@Valid @RequestBody RegisterRequest registerRequest) throws JsonProcessingException {
        log.info("Incoming Request for endpoint /restaurant/register With Time {} And request {}", LocalDateTime.now(),objectMapper.writeValueAsString(registerRequest));
        return restaurantService.register(registerRequest);
    }

    @PostMapping("/login")
    public ResponseEntity<?> loginRestaurant(@Valid @RequestBody LoginRequest loginRequest) throws JsonProcessingException {
        log.info("Incoming Request for endpoint /restaurant/login With Time {} And request {}", LocalDateTime.now(),objectMapper.writeValueAsString(loginRequest));
        return restaurantService.login(loginRequest);
    }

    @PostMapping("/profile")
    public ResponseEntity<?> fullProfile(@RequestHeader("Authorization") String authorization,@RequestBody FullProfile fullProfile) throws JsonProcessingException {
        log.info("Incoming Request for endpoint /restaurant/login With Time {} And request {}", LocalDateTime.now(),objectMapper.writeValueAsString(fullProfile));
        Restaurant restaurant= checkAuth(authorization);
        return restaurantService.fullProfile(restaurant,fullProfile);
    }

    @GetMapping("/profile")
    public ResponseEntity<?> getProfile(@RequestHeader("Authorization") String authorization) throws JsonProcessingException {
        log.info("Incoming Request for endpoint /restaurant/login With Time {}", LocalDateTime.now());
        Restaurant restaurant= checkAuth(authorization);
        return restaurantService.getProfile(restaurant);
    }

    @PostMapping("/test-token")
    public ResponseEntity<?> testToken(@RequestBody Token request) {
        Restaurant a = jwtGenerator.checkJWT(request.getToken());
        return ResponseEntity.ok().body(a);
    }

    private Restaurant checkAuth(String authorization) {
        Restaurant restaurant = jwtGenerator.checkJWT(authorization);
        if(restaurant==null){
            throw new ValidationException("Invalid Authorization");
        }
        return restaurant;
    }
}
