package com.skripsi.pointofsale.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skripsi.pointofsale.jpa.entity.Restaurant;
import com.skripsi.pointofsale.exception.ValidationException;
import com.skripsi.pointofsale.request.staff.LoginRequest;
import com.skripsi.pointofsale.request.staff.StaffRegisterRequest;
import com.skripsi.pointofsale.request.staff.UpdateRequest;
import com.skripsi.pointofsale.response.staff.RegisterResponse;
import com.skripsi.pointofsale.security.JWTGenerator;
import com.skripsi.pointofsale.service.StaffService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping("/staff")
@CrossOrigin(origins = "*")
public class StaffController {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private StaffService staffService;
    @Autowired
    private JWTGenerator jwtGenerator;

    @PostMapping("/register")
    public ResponseEntity<RegisterResponse> registerStaff(@RequestHeader("Authorization") String authorization, @Valid @RequestBody StaffRegisterRequest staffRegisterRequest) throws JsonProcessingException {
        log.info("Incoming Request for endpoint /restaurant/register With Time {} And request {}", LocalDateTime.now(),objectMapper.writeValueAsString(staffRegisterRequest));
        Restaurant restaurant= checkAuth(authorization);
        return staffService.register(staffRegisterRequest,restaurant);
    }

    @PostMapping("/login")
    public ResponseEntity<?> loginStaff(@Valid @RequestBody LoginRequest loginRequest) throws JsonProcessingException {
        log.info("Incoming Request for endpoint /restaurant/login With Time {} And request {}", LocalDateTime.now(),objectMapper.writeValueAsString(loginRequest));
        return staffService.login(loginRequest);
    }

    @GetMapping("/users")
    public ResponseEntity<?> getUsers(@RequestHeader("Authorization") String authorization,@PathVariable(name = "id",required = false) String id){
        log.info("Incoming Request for endpoint GET /users With Time {} ", LocalDateTime.now());
        Restaurant restaurant= checkAuth(authorization);
        return staffService.getUsers(restaurant,id);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<?> getUsersById(@RequestHeader("Authorization") String authorization,@PathVariable(name = "id",required = false) String id){
        log.info("Incoming Request for endpoint GET /users With Time {} ", LocalDateTime.now());
        Restaurant restaurant= checkAuth(authorization);
        return staffService.getUsers(restaurant,id);
    }

    @PostMapping("/users")
    public ResponseEntity<?> updateUsers(@RequestHeader("Authorization") String authorization, @Valid @RequestBody UpdateRequest updateRequest) {
        log.info("Incoming Request for endpoint POST /users With Time {} ", LocalDateTime.now());
        Restaurant restaurant= checkAuth(authorization);
        return staffService.updateUsers(updateRequest);
    }

    private Restaurant checkAuth(String authorization) {
        Restaurant restaurant = jwtGenerator.checkJWT(authorization);
        if(restaurant==null){
            throw new ValidationException("Invalid Authorization");
        }
        return restaurant;
    }
}
