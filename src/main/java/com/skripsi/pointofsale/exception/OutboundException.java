package com.skripsi.pointofsale.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OutboundException extends RuntimeException{
    private static final long serialVersionUID = 200L;
    private String processId;
    private String responseLog;
    public OutboundException(String message) {
        super(message);
    }
    public OutboundException(String message, String responseLog) {
        super(message);
        this.responseLog = responseLog;
    }
}

