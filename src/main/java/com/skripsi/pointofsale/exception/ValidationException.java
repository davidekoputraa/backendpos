package com.skripsi.pointofsale.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidationException extends RuntimeException {

    private static final long serialVersionUID = 200L;
    private String processId;
    public ValidationException(String message) {
        super(message);
    }
    public ValidationException(String message, String processId) {
        super(message);
        this.processId = processId;
    }

}
