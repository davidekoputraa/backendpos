package com.skripsi.pointofsale.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skripsi.pointofsale.exception.ValidationException;
import com.skripsi.pointofsale.jpa.entity.Restaurant;
import com.skripsi.pointofsale.request.PaymentGateway.PayRequest;
import com.skripsi.pointofsale.security.JWTGenerator;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.util.encoders.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

@Component
@Slf4j
public class HelperUtil {

    public static String URL = "https://api-stage.mcpayment.id/ewallet/v2/create-payment";

    public static String HASH_KEY = "UH8ZUg9pXVzbcQekF7YRs5FXpuS7yOClKA4xu8ofJNI";
    public static String ORDER_ID = "Wallet-01";

    Random random = new Random();

    @Autowired
    private JWTGenerator jwtGenerator;

    @Autowired
    private ObjectMapper objectMapper;
    public Restaurant checkAuth(String authorization) {
        Restaurant restaurant = jwtGenerator.checkJWT(authorization);
        if(restaurant==null){
            throw new ValidationException("Invalid Authorization");
        }
        return restaurant;
    }

    public String convertSha256(PayRequest payRequest){
        try {
            log.info(objectMapper.writeValueAsString(payRequest));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        String signString = HASH_KEY+payRequest.getTransactionId().toString()+ ORDER_ID;
        return hash(signString);
    }

    public static String hash(String plainText) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new RuntimeException("SHA-256 Algorithm is not found");
        }
        byte[] signatureByte = digest.digest(plainText.getBytes(StandardCharsets.UTF_8));
        return new String(Hex.encode(signatureByte));
    }
}
