package com.skripsi.pointofsale.helper;

import com.skripsi.pointofsale.exception.ValidationException;
import lombok.Getter;

@Getter
public enum OrderEnum {
    ON_ORDER("01", "Order Ongoing"),
    FINISHED_ORDER("02", "Order Finished"),
    CANCELED_ORDER("03", "Order Cancelled"),
    ;

    private final String code;
    private final String description;

    private OrderEnum(String code, String description) {
        this.code = null == code ? "" : code;
        this.description = null == description ? "" : description;
    }

    public static String getEnumDesc(String EnumCode){
        for(OrderEnum e : OrderEnum.values()){
            if(e.code.equalsIgnoreCase(EnumCode)){
                return e.getDescription();
            }
        }

        throw new ValidationException("status only 01-03");
    }
}
