package com.skripsi.pointofsale.helper;

import lombok.Getter;

@Getter
public enum PaymentChannelsEnum {
    DANA("DANA"),
    SHOPPE("SHOPPE"),
    OVO("OVO"),
    NOBU("NOBU"),
    ;

    private String paymentChannels;

    private PaymentChannelsEnum(String paymentChannels) {
        this.paymentChannels = paymentChannels;
    }

    public static boolean getPaymentChannels (String paymentChannels){
        for (PaymentChannelsEnum channelsEnum: PaymentChannelsEnum.values()) {
            if(channelsEnum.paymentChannels.equalsIgnoreCase(paymentChannels)){
                return true;
            }
        }
        return false;
    }
}
