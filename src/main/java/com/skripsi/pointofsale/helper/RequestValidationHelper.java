package com.skripsi.pointofsale.helper;

import com.skripsi.pointofsale.exception.ValidationException;
import com.skripsi.pointofsale.request.restaurant.RegisterRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RequestValidationHelper {
    // tak guna
    public static void validateRegisterRestaurant(RegisterRequest registerRequest){
        if(registerRequest.getRestaurantName().isEmpty()){
            throw new ValidationException("Register Name is Required");
        }
    }
}
