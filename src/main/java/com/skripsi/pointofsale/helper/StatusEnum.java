package com.skripsi.pointofsale.helper;

import com.skripsi.pointofsale.exception.ValidationException;
import lombok.Getter;

@Getter
public enum StatusEnum {
    ORDER_RECEIVE("01", "Order Receive"),
    PREPARE("02", "Preparing Order"),
    COOKING("03", "Order Has been Cooked"),
    SERVE("04", "Order Served"),
    ;

    private final String code;
    private final String description;

    private StatusEnum(String code, String description) {
        this.code = null == code ? "" : code;
        this.description = null == description ? "" : description;
    }

    public static String getEnumDesc(String EnumCode){
        for(StatusEnum e : StatusEnum.values()){
            if(e.code.equalsIgnoreCase(EnumCode)){
                return e.getDescription();
            }
        }

        throw new ValidationException("status only 01-04");
    }

}
