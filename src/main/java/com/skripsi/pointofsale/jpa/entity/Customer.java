package com.skripsi.pointofsale.jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.skripsi.pointofsale.helper.TimestampConverter;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.UUID;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Customer implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "customer_sequence")
    private BigInteger id;

    @JsonProperty(value = "user_id")
    private UUID userId;
    @CreatedDate
    @Convert(converter = TimestampConverter.class)
    @Column(name = "created_at", nullable = false)
    protected ZonedDateTime createdAt = ZonedDateTime.now();

    @ManyToOne
    @JoinColumn(name = "restaurant_id")
    @JsonIgnore
    private Restaurant restaurant;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_number")
    @JsonProperty(value = "phone_number")
    private String phoneNumber;

    @Column(name = "password")
    @JsonIgnore
    private String password;

}
