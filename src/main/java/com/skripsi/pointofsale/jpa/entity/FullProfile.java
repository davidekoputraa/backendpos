package com.skripsi.pointofsale.jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FullProfile implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    @OneToOne
    @MapsId
    @JoinColumn(name = "restaurant_id")
    @JsonIgnore
    private Restaurant restaurant;

    @JsonProperty("motto")
    private String motto;

    @JsonProperty("motto_detail")
    private String mottoDetail;

    @JsonProperty("restaurant_story")
    private String restaurantStory;

    @JsonProperty("open_day")
    private String openDay;

    @JsonProperty("open_time")
    private String openTime;

    @JsonProperty("location")
    private String Location;

    @JsonProperty("phone_number")
    private String phoneNumber;

    @JsonProperty("official_email")
    private String officialEmail;

    @JsonProperty("facebook_url")
    private String facebookUrl;

    @JsonProperty("instagram_url")
    private String instagramUrl;

    @JsonProperty("whatsapp_number")
    private String whatsappNumber;
}
