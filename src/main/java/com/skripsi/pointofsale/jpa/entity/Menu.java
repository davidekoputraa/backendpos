package com.skripsi.pointofsale.jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.skripsi.pointofsale.helper.TimestampConverter;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.List;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Menu implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "menu_sequence")
    private BigInteger id;

    @ManyToOne
    @JoinColumn(name = "restaurant_id")
    @JsonIgnore
    private Restaurant restaurant;

    @ManyToOne
    @JoinColumn(name = "category_id")
    @JsonIgnore
    private Category category;

    @JsonProperty("category_name")
    private String categoryName;

    @OneToMany(mappedBy = "menu")
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    private List<OrderDetail> orderDetail;

    @CreatedDate
    @Convert(converter = TimestampConverter.class)
    @Column(name = "created_at", nullable = false)
    protected ZonedDateTime createdAt = ZonedDateTime.now();

    @JsonProperty("menu_name")
    private String menuName;

    @JsonProperty("price")
    private int price;

    @JsonProperty("description")
    private String description;

    @JsonProperty("picture_code")
    private String pictureCode;

    @JsonProperty("menu_status")
    @Column(name = "menu_status")
    @ColumnDefault("true")
    private boolean status;
}
