package com.skripsi.pointofsale.jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.skripsi.pointofsale.helper.OrderEnum;
import com.skripsi.pointofsale.helper.TimestampConverter;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderBos implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "order_bos_sequence")
    private Long id;

    @JsonProperty("order_id")
    private UUID orderId=UUID.randomUUID();

    @CreatedDate
    @Convert(converter = TimestampConverter.class)
    @Column(name = "created_at", nullable = false)
    @JsonProperty("created_at")
    protected ZonedDateTime createdAt = ZonedDateTime.now();

    @Column(name = "user_id")
    @JsonProperty("user_id")
    private String userId;
    @JsonProperty("table_number")
    @Column(name = "table_number")
    private int tableNumber;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "orderBos")
    private List<OrderDetail> orderDetails = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "restaurant_id")
    @JsonIgnore
    private Restaurant restaurant;

    @Column(name = "order_done")
    @JsonProperty("order_done")
    @Builder.Default
    private Integer orderDone = 0;

    @JsonProperty("total_price")
    @Column(name = "total_price")
    private Long totalPrice;

    @Column(name = "order_status")
    @JsonProperty("order_status")
    @Builder.Default
    private String orderStatus= OrderEnum.ON_ORDER.getDescription();
}
