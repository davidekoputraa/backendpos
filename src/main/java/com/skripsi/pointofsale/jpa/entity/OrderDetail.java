package com.skripsi.pointofsale.jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.skripsi.pointofsale.helper.StatusEnum;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetail implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "order_detail_sequence")
    private Long id;

    @JsonProperty("order_detail_id")
    private String orderDetailid;

    @ManyToOne
    @JoinColumn(name = "menu_id")
    @JsonIgnore
    private Menu menu;

    @ManyToOne
    @JoinColumn(name = "orderBos_id")
    @JsonIgnore
    private OrderBos orderBos;

    @JsonProperty("menu_name")
    private String name;

    private Long amount;

    @JsonProperty("quantity")
    private Long qty;
    @Builder.Default
    private String description="-";

    @Builder.Default
    private String status = StatusEnum.ORDER_RECEIVE.getDescription();

}
