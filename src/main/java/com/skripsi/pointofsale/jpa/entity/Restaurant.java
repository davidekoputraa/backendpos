package com.skripsi.pointofsale.jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.skripsi.pointofsale.helper.TimestampConverter;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "restaurant")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Restaurant implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "restaurant_sequence")
    private BigInteger id;

    @CreatedDate
    @Convert(converter = TimestampConverter.class)
    @Column(name = "created_at", nullable = false)
    @JsonIgnore
    protected ZonedDateTime createdAt = ZonedDateTime.now();
    @Column(name = "restaurant_id")
    private String restaurantId;
    @Column(name = "restaurant_name")
    private String restaurant_name;
    @Column(name = "email")
    private String email;
    @Column(name = "phone_number")
    private String phoneNumber;
    @Column(name = "password")
    private String password;

    @OneToMany(mappedBy = "restaurant")
    private List<Menu> menus = new ArrayList<>();

    @OneToMany(mappedBy = "restaurant")
    private List<Category> categories = new ArrayList<>();

    @OneToMany(mappedBy = "restaurant")
    private List<OrderBos> orderBos = new ArrayList<>();

    @OneToOne(mappedBy = "restaurant", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private FullProfile fullProfile;

    @OneToMany(mappedBy = "restaurant",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Staff> staff = new ArrayList<>();

    private String tokenInfo;
}
