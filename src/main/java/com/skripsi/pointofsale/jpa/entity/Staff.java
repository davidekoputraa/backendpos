package com.skripsi.pointofsale.jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.skripsi.pointofsale.helper.TimestampConverter;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.ZonedDateTime;

@Entity
@Table(name = "staff")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Staff implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "staff_sequence")
    private BigInteger id;

    @CreatedDate
    @Convert(converter = TimestampConverter.class)
    @Column(name = "created_at", nullable = false)
    protected ZonedDateTime createdAt = ZonedDateTime.now();
    @Column(name = "staff_id")
    @JsonProperty("staff_id")
    private String staffId;

    @ManyToOne
    @JoinColumn(name = "restaurant_id")
    @JsonIgnore
    private Restaurant restaurant;
    @Column(name = "updated_at")
    @JsonProperty("updated_at")
    private ZonedDateTime updatedAt;
    @JsonProperty("staff_name")
    @Column(name = "staff_name")
    private String staffName;
    @Column(name = "email")
    private String email;
    @Column(name = "phone_number")
    @JsonProperty("phone_number")
    private String phoneNumber;
    @Column(name = "password")
    @JsonIgnore
    private String password;
    @Column(name = "role")
    private String role;

    @Column(name = "picture_code")
    @JsonProperty("picture_code")
    private String pictureCode;
}
