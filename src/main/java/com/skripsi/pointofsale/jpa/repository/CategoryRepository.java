package com.skripsi.pointofsale.jpa.repository;

import com.skripsi.pointofsale.jpa.entity.Category;
import com.skripsi.pointofsale.jpa.entity.Menu;
import com.skripsi.pointofsale.jpa.entity.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category,String> {
    Category findByCategoryNameAndRestaurant(String category, Restaurant restaurant);
    List<Category> findByRestaurant(Restaurant restaurant);
}
