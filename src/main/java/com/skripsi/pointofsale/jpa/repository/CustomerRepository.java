package com.skripsi.pointofsale.jpa.repository;

import com.skripsi.pointofsale.jpa.entity.Customer;
import com.skripsi.pointofsale.jpa.entity.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,String> {
    Customer findByEmailAndRestaurant(String email, Restaurant restaurant);
    Customer findById(BigInteger id);
    List<Customer> findByRestaurantId(BigInteger restaurantId);
}
