package com.skripsi.pointofsale.jpa.repository;

import com.skripsi.pointofsale.jpa.entity.FullProfile;
import com.skripsi.pointofsale.jpa.entity.Menu;
import com.skripsi.pointofsale.jpa.entity.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FullProfileRepository extends JpaRepository<FullProfile,String> {
    FullProfile findByRestaurant(Restaurant restaurant);
}
