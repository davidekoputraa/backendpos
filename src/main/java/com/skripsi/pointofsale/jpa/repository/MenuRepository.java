package com.skripsi.pointofsale.jpa.repository;

import com.skripsi.pointofsale.jpa.entity.Category;
import com.skripsi.pointofsale.jpa.entity.Menu;
import com.skripsi.pointofsale.jpa.entity.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Repository
public interface MenuRepository extends JpaRepository<Menu,String> {
    Menu findByMenuNameAndRestaurant(String menuName, Restaurant restaurant);

    Optional<Menu> findById(BigInteger id);

    List<Menu> findByRestaurantId(BigInteger restaurantId);
    List<Menu> findByRestaurantIdAndCategory(BigInteger restaurantId, Category category);

}
