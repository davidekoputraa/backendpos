package com.skripsi.pointofsale.jpa.repository;

import com.skripsi.pointofsale.jpa.entity.OrderBos;
import com.skripsi.pointofsale.jpa.entity.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface OrderBosRepository extends JpaRepository <OrderBos,String> {
    Optional<OrderBos> findById(Long id);
    Optional<OrderBos> findByOrderIdAndRestaurant(UUID id,Restaurant restaurant);
    Optional<List<OrderBos>>findByRestaurantAndUserId(Restaurant restaurant, String userId);

    Optional<OrderBos>findByRestaurantAndOrderId(Restaurant restaurant, UUID orderId);
    Optional<List<OrderBos>>findByRestaurant(Restaurant restaurant);

    List<OrderBos>findByRestaurantAndOrderStatusIn(Restaurant restaurant,List<String> orderStatus);

    List<OrderBos>findByRestaurantAndUserIdAndOrderStatusIn(Restaurant restaurant, String userId,List<String> orderStatus);

    @Query("SELECT o FROM OrderBos o WHERE o.restaurant = :restaurant " +
            "AND o.createdAt BETWEEN :startDate AND :endDate")
    List<OrderBos> findByRestaurantAndDateBetween(@Param("restaurant") Restaurant restaurant,
                                                  @Param("startDate") ZonedDateTime startDate,
                                                  @Param("endDate") ZonedDateTime endDate);
}
