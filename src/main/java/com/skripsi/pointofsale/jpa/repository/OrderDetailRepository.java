package com.skripsi.pointofsale.jpa.repository;

import com.skripsi.pointofsale.jpa.entity.OrderBos;
import com.skripsi.pointofsale.jpa.entity.OrderDetail;
import com.skripsi.pointofsale.jpa.entity.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail,String> {
    OrderDetail findByOrderDetailid(String id);

    OrderDetail findByOrderDetailidAndOrderBos(String id, OrderBos orderBos);
}
