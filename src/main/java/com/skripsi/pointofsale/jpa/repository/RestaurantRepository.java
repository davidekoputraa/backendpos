package com.skripsi.pointofsale.jpa.repository;

import com.skripsi.pointofsale.jpa.entity.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant,String> {
    Restaurant findByRestaurantId(String restauranId);
}
