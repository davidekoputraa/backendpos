package com.skripsi.pointofsale.jpa.repository;

import com.skripsi.pointofsale.jpa.entity.Restaurant;
import com.skripsi.pointofsale.jpa.entity.Staff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface StaffRepository extends JpaRepository<Staff,String> {
    Staff findByEmail(String email);
    Staff findByEmailAndRestaurant(String email,Restaurant restaurant);
    List<Staff> findByRestaurant(Restaurant restaurant);
    Staff findByStaffId(String staffId);
    List<Staff> findByRestaurantId(BigInteger restaurantId);
}
