package com.skripsi.pointofsale.request.PaymentGateway;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.UUID;

@Data
public class PayRequest implements Serializable {
    @JsonProperty("transaction_id")
    @NotBlank(message = "transaction_id is required")
    private UUID transactionId;

    @JsonProperty("amount")
    @NotBlank(message = "amount is required")
    private Long amount;

    @JsonProperty("transaction_description")
    @NotBlank(message = "transaction_description is required")
    private String transactionDescription;

    @JsonProperty("phone_number")
    @NotBlank(message = "phone_number is required")
    private String phoneNumber;

    @JsonProperty("return_url")
    @NotBlank(message = "return_url is required")
    private String returnUrl;

    @JsonProperty("payment_channel")
    @NotBlank(message = "payment_channel is required")
    private String paymentChannels;
}
