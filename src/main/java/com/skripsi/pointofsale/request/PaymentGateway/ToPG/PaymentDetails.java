package com.skripsi.pointofsale.request.PaymentGateway.ToPG;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class PaymentDetails implements Serializable {

    @JsonProperty("amount")
    private Long amount;

    @JsonProperty("transaction_description")
    private String transactionDescription;

    @JsonProperty("is_customer_paying_fee")
    private boolean isCustomerPayingFee=false;
}
