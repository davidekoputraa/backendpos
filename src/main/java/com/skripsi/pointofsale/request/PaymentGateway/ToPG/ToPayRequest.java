package com.skripsi.pointofsale.request.PaymentGateway.ToPG;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class ToPayRequest implements Serializable {
    @JsonProperty("external_id")
    private String externalId;

    @JsonProperty("order_id")
    private String orderId;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("payment_method")
    private String paymentMethod;

    @JsonProperty("payment_channel")
    private String paymentChannel;

    @JsonProperty("payment_details")
    private PaymentDetails paymentDetail;

    @JsonProperty("customer_details")
    private CustomerDetails customerDetails;

    @JsonProperty("wallet_details")
    private WalletDetails walletDetails;

    @JsonProperty("return_url")
    private String returnUrl;

    @JsonProperty("callback_url")
    private String callbackUrl;

}
