package com.skripsi.pointofsale.request.PaymentGateway.ToPG;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class WalletDetails implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("id_type")
    private String idType = "hp";
}
