package com.skripsi.pointofsale.request.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigInteger;

@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateCustomerRequest implements Serializable {
    @JsonProperty("id")
    private BigInteger id;

    @JsonProperty("name")
    private String name;
    @Email(message = "Email is not valid")
    @JsonProperty("email")
    private String email;


    @JsonProperty("phone_number")
    private String phoneNumber;
}
