package com.skripsi.pointofsale.request.menu;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.Column;
import java.io.Serializable;
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class StatusMenu implements Serializable {

    @JsonProperty("menu_name")
    private String menuName;

    @JsonProperty("menu_status")
    private boolean status;
}
