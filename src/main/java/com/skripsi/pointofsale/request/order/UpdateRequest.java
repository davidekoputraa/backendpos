package com.skripsi.pointofsale.request.order;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.skripsi.pointofsale.jpa.entity.OrderDetail;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateRequest implements Serializable {
    @JsonProperty("order_id")
    @NotBlank(message = "order_id is required")
    private String orderId;
    @JsonProperty("table_number")
    @NotBlank(message = "tableNumber is required")
    private int tableNumber;

    @JsonProperty("user_id")
    @NotBlank(message = "user_id is required")
    private String userId;
    @JsonProperty("order_detail")
    private List<OrderDetail> orderDetails;

}