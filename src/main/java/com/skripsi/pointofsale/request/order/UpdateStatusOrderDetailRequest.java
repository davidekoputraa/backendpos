package com.skripsi.pointofsale.request.order;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateStatusOrderDetailRequest implements Serializable {

    @JsonProperty("order_id")
    @NotBlank(message = "order is required")
    private UUID orderId;

    @JsonProperty("order_detail_id")
    @NotBlank(message = "order_detail_id is required")
    private String orderDetailId;

    @JsonProperty("status")
    private String statusCode;

}
