package com.skripsi.pointofsale.request.order;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.skripsi.pointofsale.helper.StatusEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateStatusOrderRequest implements Serializable {

    @JsonProperty("order_id")
    @NotBlank(message = "order is required")
    private UUID orderId;

    @JsonProperty("status")
    private String statusCode;

}
