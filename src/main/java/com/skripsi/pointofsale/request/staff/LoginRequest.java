package com.skripsi.pointofsale.request.staff;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginRequest implements Serializable {

    @NotBlank(message = "email is required")
    @Email(message = "Email is not valid")
    @JsonProperty("email")
    private String email;
    @NotBlank(message = "password is required")
    @JsonProperty("password")
    private String password;

}
