package com.skripsi.pointofsale.request.staff;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class StaffRegisterRequest implements Serializable {

    @NotBlank(message = "name is required")
    @JsonProperty("name")
    private String name;
    @NotBlank(message = "email is required")
    @Email(message = "Email is not valid")
    @JsonProperty("email")
    private String email;

    @NotBlank(message = "phone_number is required")
    @JsonProperty("phone_number")
    private String phoneNumber;

    @JsonProperty("role")
    private String role;

    @NotBlank(message = "password is required")
    @JsonProperty("password")
    private String password;
}
