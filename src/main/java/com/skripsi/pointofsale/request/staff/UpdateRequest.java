package com.skripsi.pointofsale.request.staff;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateRequest implements Serializable {

    @JsonProperty("staff_id")
    @NotBlank(message = "staff_id is required")
    private String staffId;

    @JsonProperty("staff_name")
    private String name;

    @Email(message = "Email is not valid")
    @JsonProperty("email")
    private String email;

    @JsonProperty("phone_number")
    private String phoneNumber;

    @JsonProperty("role")
    private String role;

}
