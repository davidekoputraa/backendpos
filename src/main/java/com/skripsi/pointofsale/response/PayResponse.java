package com.skripsi.pointofsale.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.UUID;

@Data
public class PayResponse implements Serializable {

    @JsonProperty("transaction_id")
    private UUID transactionId;

    @JsonProperty("external_id")
    private String externalId;

    @JsonProperty("order_id")
    private String order_id;

    @JsonProperty("wallet_response")
    private UUID walletResponse;

    @JsonProperty("transaction_status")
    private String transactionStatus;

    @JsonProperty("responseCode")
    private String responseCode;


}
