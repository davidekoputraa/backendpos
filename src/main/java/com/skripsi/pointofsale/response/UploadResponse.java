package com.skripsi.pointofsale.response;

public class UploadResponse {
    private String fileName;
    private byte[] compressedImageBytes;

    public UploadResponse(String fileName, byte[] compressedImageBytes) {
        this.fileName = fileName;
        this.compressedImageBytes = compressedImageBytes;
    }

    public String getFileName() {
        return fileName;
    }

    public byte[] getCompressedImageBytes() {
        return compressedImageBytes;
    }
}
