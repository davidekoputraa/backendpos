package com.skripsi.pointofsale.response.error;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ErrorResponse {

    @JsonProperty("status")
    int status;

    @JsonProperty("response_message")
    String responseMessage;

}
