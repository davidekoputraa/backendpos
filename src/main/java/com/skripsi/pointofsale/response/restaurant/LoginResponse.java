package com.skripsi.pointofsale.response.restaurant;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@Builder
public class LoginResponse implements Serializable {
    @JsonProperty("status")
    int status;

    @JsonProperty("response_message")
    String responseMessage;

    @JsonProperty("token")
    private String token;

    @JsonProperty("token_time")
    private String tokenTime;

    @JsonProperty("restaurant_name")
    private String restaurantName;

    @JsonProperty("restaurant_id")
    private String restaurantId;
}
