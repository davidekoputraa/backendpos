package com.skripsi.pointofsale.response.restaurant;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@Builder
public class RegisterResponse implements Serializable {


    @JsonProperty("restaurant_id")
    private String restaurantId;

}
