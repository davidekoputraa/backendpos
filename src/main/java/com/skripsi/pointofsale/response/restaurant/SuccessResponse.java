package com.skripsi.pointofsale.response.restaurant;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SuccessResponse {

    @JsonProperty("role")
    private String role;

    @JsonProperty("staff_id")
    private String staffId;

    @JsonProperty("name")
    private String staffName;

    @JsonProperty("status")
    int status;

    @JsonProperty("response_message")
    String responseMessage;

    @JsonProperty("token")
    String token;

    @JsonProperty("restaurant_id")
    String restaurantId;

    @JsonProperty("restaurant_name")
    String restaurantName;

}
