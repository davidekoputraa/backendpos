package com.skripsi.pointofsale.response.staff;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@Builder
public class LoginResponse implements Serializable {

    @JsonProperty("status")
    int status;

    @JsonProperty("response_message")
    String responseMessage;
    @JsonProperty("token")
    private String token;

}
