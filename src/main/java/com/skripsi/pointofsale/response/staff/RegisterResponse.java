package com.skripsi.pointofsale.response.staff;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@Builder
public class RegisterResponse implements Serializable {


    @JsonProperty("staff_id")
    private String staffId;

    @JsonProperty("role")
    private String role;

}
