package com.skripsi.pointofsale.response.staff;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SuccessResponse {
    @JsonProperty("status")
    int status;

    @JsonProperty("response_message")
    String responseMessage;
}
