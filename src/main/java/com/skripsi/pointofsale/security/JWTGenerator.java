package com.skripsi.pointofsale.security;

import com.skripsi.pointofsale.jpa.entity.Restaurant;
import com.skripsi.pointofsale.jpa.repository.RestaurantRepository;
import com.skripsi.pointofsale.exception.ValidationException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Component
@Slf4j
public class JWTGenerator {
    @Autowired
    RestaurantRepository restaurantRepository;
    public String generateToken(String username) {
        Calendar calendar = Calendar.getInstance();
        Date currentDate = new Date();
        calendar.setTime(currentDate);
        calendar.add(Calendar.YEAR, 1);
        Date expireDate = calendar.getTime();

        return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.JWT_SECRET)
                .compact();
    }

    public Date getExpirationFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(SecurityConstants.JWT_SECRET)
                .parseClaimsJws(token)
                .getBody();
        Date expirationDate = claims.getExpiration();

        // Convert the Date object to a formatted string with date and time
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String formattedDate = sdf.format(expirationDate);

        try {
            return sdf.parse(formattedDate);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getUsernameFromJWT(String token){
        Claims claims= Jwts.parser()
                .setSigningKey(SecurityConstants.JWT_SECRET)
                .parseClaimsJws(token)
                .getBody();
        return claims.getSubject();
    }
    public boolean validateToken(String token){
        try{
            Jwts.parser().setSigningKey(SecurityConstants.JWT_SECRET).parseClaimsJws(token);
            return true;
        }catch (Exception ex){
            throw new ValidationException("YOUR ACCOUNT WAS EXPIRE OR INCORRECT");
        }
    }

    public Restaurant checkJWT(String header){
        if(validateToken(header)){
            String username =  getUsernameFromJWT(header);
            Restaurant restaurant = restaurantRepository.findByRestaurantId(username);
            if(restaurant==null){
                return null;
            }
            return restaurant;
        }else{
            return  null;
        }
    }


    public static void main(String[] args) {
        Claims claims= Jwts.parser()
                .setSigningKey(SecurityConstants.JWT_SECRET)
                .parseClaimsJws("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0aW5nLTM3MzFmOWRkLTZjMTYtNGM2OS1iZTA2LWI1MDkyNGM2YWRmYyIsImlhdCI6MTY4NjQ3OTI0OCwiZXhwIjoxMTgwNTM1NDc0MDEwMjB9.Kz8kFzrASWbMrzYOhu0Nt1bVKB8HgzeMdZe61J3St4DTunj5UivqqLnGGh1vEPbzaXgLa7q3fIPpqLgTsu4HYg")
                .getBody();
        String a = claims.getSubject();
        log.info(a);
    }
}

