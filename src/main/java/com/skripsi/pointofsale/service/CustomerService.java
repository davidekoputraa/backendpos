package com.skripsi.pointofsale.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skripsi.pointofsale.exception.ValidationException;
import com.skripsi.pointofsale.jpa.entity.Customer;
import com.skripsi.pointofsale.jpa.entity.Restaurant;
import com.skripsi.pointofsale.jpa.repository.CustomerRepository;
import com.skripsi.pointofsale.jpa.repository.RestaurantRepository;
import com.skripsi.pointofsale.request.customer.UpdateCustomerRequest;
import com.skripsi.pointofsale.request.staff.LoginRequest;
import com.skripsi.pointofsale.request.staff.StaffRegisterRequest;
import com.skripsi.pointofsale.response.restaurant.SuccessResponse;
import com.skripsi.pointofsale.security.JWTGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class CustomerService {
    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    RestaurantRepository restaurantRepository;

    @Autowired
    private JWTGenerator jwtGenerator;
    public ResponseEntity<String> register(StaffRegisterRequest staffRegisterRequest, Restaurant restaurant) throws JsonProcessingException {
            String encryptedPassword = getEncryptedPassword(staffRegisterRequest.getPassword());
            Customer customer = Customer.builder()
                .phoneNumber(staffRegisterRequest.getPhoneNumber())
                .createdAt(ZonedDateTime.now())
                .userId(UUID.randomUUID())
                .name(staffRegisterRequest.getName())
                .email(staffRegisterRequest.getEmail())
                .password(encryptedPassword)
                .restaurant(restaurant)
                .build();
        customerRepository.save(customer);
        return ResponseEntity.ok().body("Succesfull Register");
    }

    public ResponseEntity<?> login(Restaurant restaurant,LoginRequest loginRequest){
        Customer customer = customerRepository.findByEmailAndRestaurant(loginRequest.getEmail(),restaurant);
        if(customer==null){
            throw new ValidationException("wrong customer or restauran");
        }
        if(!checkPassword(loginRequest.getPassword(),customer.getPassword())){
            throw new ValidationException("wrong password");
        }
        SuccessResponse successResponse = SuccessResponse.builder().responseMessage("SUCCESS").status(HttpStatus.OK.value()).build();
        return ResponseEntity.ok().body(successResponse);
    }
    public ResponseEntity<?> getUsers(Restaurant restaurant){
        List<Customer> listCustomer= customerRepository.findByRestaurantId(restaurant.getId());
        if(listCustomer.isEmpty()){
            throw new ValidationException("no customer");
        }
        List<Customer> listCustomers = createStaffList(listCustomer);
        return ResponseEntity.ok().body(listCustomers);
    }

    public ResponseEntity<?> updateUsers(UpdateCustomerRequest updateRequest){
        Customer customer  = customerRepository.findById(updateRequest.getId());
        if(customer == null){
            throw new ValidationException("invalid customer id");
        }
        updateDataUser(updateRequest, customer);
        return ResponseEntity.ok().body(customer);
    }

    private void updateDataUser(UpdateCustomerRequest updateRequest, Customer customer) {
        if(updateRequest.getName()!=null){
            customer.setName(updateRequest.getName());
            customer.setUpdatedAt(ZonedDateTime.now());
        }
        if(updateRequest.getEmail()!=null){
            customer.setEmail(updateRequest.getEmail());
            customer.setUpdatedAt(ZonedDateTime.now());
        }
        if(updateRequest.getPhoneNumber()!=null){
            customer.setPhoneNumber(updateRequest.getPhoneNumber());
            customer.setUpdatedAt(ZonedDateTime.now());
        }
        customerRepository.save(customer);
    }

    private List<Customer> createStaffList(List<Customer> customers) {
        List<Customer> itemEntities = new ArrayList<>();
        for (Customer item : customers) {
            itemEntities.add(Customer.builder().id(item.getId()).userId(item.getUserId()).createdAt(item.getCreatedAt()).name(item.getName()).email(item.getEmail()).phoneNumber(item.getPhoneNumber()).build());
        }

        return itemEntities;
    }
    private static String getEncryptedPassword(String password) {
        BCryptPasswordEncoder crypt = new BCryptPasswordEncoder();
        String encryptedPassword = crypt.encode(password);
        log.info("pass{} | encrpt{}",password,encryptedPassword);
        return encryptedPassword;
    }

    private static boolean checkPassword(String password,String decodePassword) {
        BCryptPasswordEncoder crypt = new BCryptPasswordEncoder();
        boolean encryptedPassword = crypt.matches(password,decodePassword);
        return encryptedPassword;
    }
}
