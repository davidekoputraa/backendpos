package com.skripsi.pointofsale.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import com.skripsi.pointofsale.exception.ValidationException;
import com.skripsi.pointofsale.jpa.entity.Menu;
import com.skripsi.pointofsale.jpa.entity.Restaurant;
import com.skripsi.pointofsale.jpa.entity.Staff;
import com.skripsi.pointofsale.jpa.repository.MenuRepository;
import com.skripsi.pointofsale.jpa.repository.StaffRepository;
import com.skripsi.pointofsale.request.restaurant.RegisterRequest;
import com.skripsi.pointofsale.response.restaurant.RegisterResponse;
import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.UploadObjectArgs;
import io.minio.errors.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class MainService {
    String a ="D:\\MC PAYMENT\\BitBucket\\pointofsale\\src\\main\\resources";
    String bucketName = "pointofsales";
    @Autowired
    private AmazonS3 s3Client;

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private StaffRepository staffRepository;


    public ResponseEntity<?> uploadFile(MultipartFile file, Restaurant restaurant, String type, String name) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        byte[] compressedImageBytes = compressAndResizeImage(file.getBytes(), 512);
        String fileName = restaurant.getRestaurantId() + "_" + name;

        if (type!=null && type.equalsIgnoreCase("menu")) {
            Menu menu = menuRepository.findByMenuNameAndRestaurant(name, restaurant);
            if (menu == null) {
                throw new ValidationException("menu doesn't exist");
            }
            menu.setPictureCode(fileName);
            menuRepository.save(menu);
        }
        if(type!=null && type.equalsIgnoreCase("employee")){
            Staff staff = staffRepository.findByStaffId(name);
            if(staff==null){
                throw new ValidationException("staff doesn't exist");
            }
            staff.setPictureCode(fileName);
            staffRepository.save(staff);
        }

        // Upload the compressed image to AWS S3
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(compressedImageBytes.length);
        s3Client.putObject(new PutObjectRequest("pointofsales", fileName, new ByteArrayInputStream(compressedImageBytes), metadata));

        // Return the compressed image in the response
        return ResponseEntity.ok()
                .body(fileName);
    }

    private byte[] compressAndResizeImage(byte[] imageBytes, int maxWidth) throws IOException {
        BufferedImage image = ImageIO.read(new ByteArrayInputStream(imageBytes));

        // Resize the image to the desired maximum width while preserving the aspect ratio
        int width = image.getWidth();
        int height = image.getHeight();
        if (width > maxWidth) {
            double ratio = (double) maxWidth / width;
            int newWidth = maxWidth;
            int newHeight = (int) (height * ratio);

            Image resizedImage = image.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
            BufferedImage resizedBufferedImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
            resizedBufferedImage.getGraphics().drawImage(resizedImage, 0, 0, null);

            image = resizedBufferedImage;
        }

        // Compress the image
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", baos);

        return baos.toByteArray();
    }

    public ResponseEntity<byte[]> downloadFile(String fileName) {
        S3Object s3Object = s3Client.getObject(bucketName, fileName);
        S3ObjectInputStream inputStream = s3Object.getObjectContent();
        try {
            byte[] content = IOUtils.toByteArray(inputStream);
            HttpHeaders headers =   new HttpHeaders();
            headers.setContentType(MediaType.IMAGE_JPEG); // Replace with the appropriate content type if needed
            headers.setContentDispositionFormData("attachment", fileName);
            return new ResponseEntity<>(content, headers, HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }


    public String deleteFile(String fileName) {
        s3Client.deleteObject(bucketName, fileName);
        return fileName + " removed ...";
    }

    public List<String> listFile() {
        ListObjectsV2Result listObjectsV2Result = s3Client.listObjectsV2(bucketName);
        return listObjectsV2Result.getObjectSummaries().stream().map(o->o.getKey()).collect(Collectors.toList());
    }


    private File convertMultiPartFileToFile(MultipartFile file) {
        File convertedFile = new File(file.getOriginalFilename());
        try (FileOutputStream fos = new FileOutputStream(convertedFile)) {
            fos.write(file.getBytes());
        } catch (IOException e) {
            log.error("Error converting multipartFile to file", e);
        }
        return convertedFile;
    }

    private File compressAndResizeImage(File imageFile, int maxWidth) throws IOException {
        BufferedImage image = ImageIO.read(imageFile);

        // Resize the image to the desired maximum width while preserving the aspect ratio
        int width = image.getWidth();
        int height = image.getHeight();
        if (width > maxWidth) {
            double ratio = (double) maxWidth / width;
            int newWidth = maxWidth;
            int newHeight = (int) (height * ratio);

            Image resizedImage = image.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
            BufferedImage resizedBufferedImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
            resizedBufferedImage.getGraphics().drawImage(resizedImage, 0, 0, null);

            image = resizedBufferedImage;
        }

        // Compress the image
        File compressedFile = new File(imageFile.getAbsolutePath() + "_compressed.jpg");
        ImageIO.write(image, "jpg", compressedFile);

        return compressedFile;
    }

    public MinioClient minioClient() throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        MinioClient minioClient =MinioClient.builder()
                .endpoint("103.52.115.40/pointofsales")
                .credentials("N3OUZ6HF2FY961AQHDG8", "B93eOMj3g6Q3X5XvJBNiL4gA0oOPMw6LU3gv5yC4")
                .build();
        boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket("point_of_sales").build());
        if (!found) {
            // Make a new bucket called 'asiatrip'.
            minioClient.makeBucket(MakeBucketArgs.builder().bucket("point_of_sales").build());
        } else {
            System.out.println("Bucket 'asiatrip' already exists.");
        }
        return minioClient;
    }
}
