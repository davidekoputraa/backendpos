package com.skripsi.pointofsale.service;

import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.errors.*;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class MinioService {
    @Bean
    public MinioClient minioClient() throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
            MinioClient minioClient =MinioClient.builder()
                    .endpoint("https://is3.cloudhost.id/pointofsales")
                    .credentials("N3OUZ6HF2FY961AQHDG8", "B93eOMj3g6Q3X5XvJBNiL4gA0oOPMw6LU3gv5yC4")
                    .build();
            boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket("point_of_sales").build());
            if (!found) {
                // Make a new bucket called 'asiatrip'.
                minioClient.makeBucket(MakeBucketArgs.builder().bucket("point_of_sales").build());
            } else {
                System.out.println("Bucket 'asiatrip' already exists.");
            }
            return minioClient;
    }
}
