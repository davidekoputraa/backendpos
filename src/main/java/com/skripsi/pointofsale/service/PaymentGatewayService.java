package com.skripsi.pointofsale.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skripsi.pointofsale.exception.OutboundException;
import com.skripsi.pointofsale.exception.ValidationException;
import com.skripsi.pointofsale.helper.HelperUtil;
import com.skripsi.pointofsale.helper.PaymentChannelsEnum;
import com.skripsi.pointofsale.jpa.entity.Restaurant;
import com.skripsi.pointofsale.request.PaymentGateway.PayRequest;
import com.skripsi.pointofsale.response.PayResponse;
import com.skripsi.pointofsale.request.PaymentGateway.ToPG.CustomerDetails;
import com.skripsi.pointofsale.request.PaymentGateway.ToPG.PaymentDetails;
import com.skripsi.pointofsale.request.PaymentGateway.ToPG.ToPayRequest;
import com.skripsi.pointofsale.request.PaymentGateway.ToPG.WalletDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PaymentGatewayService {

    @Autowired
    RestTemplate restTemplate;
    @Autowired
    private HelperUtil helperUtil;


    @Autowired
    ObjectMapper objectMapper;

    public ResponseEntity<PayResponse> pay(Restaurant restaurant, PayRequest payRequest) throws JsonProcessingException {
        if (!PaymentChannelsEnum.getPaymentChannels(payRequest.getPaymentChannels())){
            String validChannels = Arrays.stream(PaymentChannelsEnum.values())
                    .map(Enum::name)
                    .collect(Collectors.joining(", "));
            throw new ValidationException("payment channels only : "+validChannels);
        }
        String url = helperUtil.URL;
        String autorization = helperUtil.convertSha256(payRequest);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic TUNQMjAyMjAzMDE5MzoweDAwNjIxMmRhZTY0ODc2YWEyNQ==");
        headers.add("x-req-signature", autorization);
        headers.add("x-version", "v3");
        PaymentDetails paymentDetails = PaymentDetails.builder()
                .amount(payRequest.getAmount())
                .transactionDescription(payRequest.getTransactionDescription()).build();
        CustomerDetails customerDetails = CustomerDetails.builder()
                .email("test@gmail.com")
                .phone(payRequest.getPhoneNumber())
                .fullName("test").build();
        WalletDetails walletDetails = WalletDetails.builder()
                .idType(payRequest.getPhoneNumber()).build();
        ToPayRequest toPayRequest = ToPayRequest.builder()
                .externalId(payRequest.getTransactionId().toString())
                .orderId(helperUtil.ORDER_ID)
                .currency("IDR")
                .paymentMethod("wallet")
                .paymentChannel(payRequest.getPaymentChannels())
                .paymentDetail(paymentDetails)
                .customerDetails(customerDetails)
                .walletDetails(walletDetails)
                .returnUrl("https://david21.free.beeceptor.com")
                .callbackUrl(payRequest.getReturnUrl())
                .build();
        HttpEntity<ToPayRequest> request = new HttpEntity<>(toPayRequest, headers);
        ResponseEntity<PayResponse> response = null;
        try {
            log.info("Get PG Data at {} and body {}", url, objectMapper.writeValueAsString(request));
            response = restTemplate.postForEntity(
                    url, request, PayResponse.class);
            log.info("Success get data from PG response: {}", objectMapper.writeValueAsString(response));
            return ResponseEntity.ok(response.getBody());
        } catch (RestClientResponseException e) {
            log.error("Failed to get data with Error: {}", e.getResponseBodyAsString());
            throw new OutboundException(e.getMessage());
        } catch (Exception e) {
            log.error("Unknown Error, data with Error: {}", e.getMessage());
            throw new OutboundException(e.getMessage());
        }
    }
}
