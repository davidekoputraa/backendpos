package com.skripsi.pointofsale.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skripsi.pointofsale.helper.OrderEnum;
import com.skripsi.pointofsale.helper.StatusEnum;
import com.skripsi.pointofsale.jpa.entity.*;
import com.skripsi.pointofsale.jpa.repository.*;
import com.skripsi.pointofsale.exception.ValidationException;
import com.skripsi.pointofsale.request.menu.MenuRequest;
import com.skripsi.pointofsale.request.menu.StatusMenu;
import com.skripsi.pointofsale.request.order.*;
import com.skripsi.pointofsale.request.restaurant.LoginRequest;
import com.skripsi.pointofsale.request.restaurant.RegisterRequest;
import com.skripsi.pointofsale.request.staff.StaffRegisterRequest;
import com.skripsi.pointofsale.response.restaurant.LoginResponse;
import com.skripsi.pointofsale.response.restaurant.RegisterResponse;
import com.skripsi.pointofsale.security.JWTGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

@Service
@Slf4j
public class RestaurantService {
    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    RestaurantRepository restaurantRepository;

    @Autowired
    MenuRepository menuRepository;

    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    OrderBosRepository orderBosRepository;

    @Autowired
    OrderDetailRepository orderDetailRepository;

    @Autowired
    FullProfileRepository fullProfileRepository;

    @Autowired
    JWTGenerator jwtGenerator;

    @Autowired
    private StaffService staffService;

    @Autowired
    private StaffRepository staffRepository;

    public ResponseEntity<RegisterResponse> register(RegisterRequest registerRequest) throws JsonProcessingException {
        String restaurantId = registerRequest.getRestaurantName().toLowerCase() + "-" + UUID.randomUUID();
        String encryptedPassword = getEncryptedPassword(registerRequest.getPassword());
        Staff checkEmail =staffRepository.findByEmail(registerRequest.getEmail());
        if(checkEmail!=null){
            throw new ValidationException("email already register, please use different email");
        }
        Restaurant restaurant = Restaurant.builder().
                createdAt(ZonedDateTime.now()).
                restaurantId(restaurantId).
                restaurant_name(registerRequest.getRestaurantName()).
                email(registerRequest.getEmail()).
                password(encryptedPassword).
                phoneNumber(registerRequest.getPhoneNumber()).build();
        String jwtToken = jwtGenerator.generateToken(restaurant.getRestaurantId());
        restaurant.setTokenInfo(jwtToken);
        restaurantRepository.save(restaurant);
        StaffRegisterRequest managerRequest = StaffRegisterRequest.builder()
                .email(registerRequest.getEmail())
                .name(registerRequest.getRestaurantName())
                .role("Manager")
                .password(registerRequest.getPassword())
                .phoneNumber(registerRequest.getPhoneNumber())
                .build();
        staffService.register(managerRequest,restaurant);
        RegisterResponse registerResponse = RegisterResponse.builder().restaurantId(restaurantId).build();
        return ResponseEntity.ok().body(registerResponse);
    }

    public ResponseEntity<?> login(LoginRequest loginRequest) {
        Restaurant restaurant = restaurantRepository.findByRestaurantId(loginRequest.getRestaurantId());
        if (restaurant == null) {
            throw new ValidationException("wrong restaurant_id");
        }
        if (!checkPassword(loginRequest.getPassword(), restaurant.getPassword())) {
            throw new ValidationException("wrong password");
        }
        String jwtToken = null;
        if (restaurant.getTokenInfo() == null) {
            jwtToken = jwtGenerator.generateToken(loginRequest.getRestaurantId());
            restaurant.setTokenInfo(jwtToken);
            restaurantRepository.save(restaurant);
        } else {
            jwtToken = restaurant.getTokenInfo();
        }
        Date expirationDate = jwtGenerator.getExpirationFromJWT(jwtToken);
        jwtGenerator.validateToken(jwtToken);
        LoginResponse loginResponse = LoginResponse.builder().responseMessage("SUCCESS").restaurantId(restaurant.getRestaurantId()).restaurantName(restaurant.getRestaurant_name()).tokenTime(String.valueOf(expirationDate)).status(HttpStatus.OK.value()).token(jwtToken).build();
        return ResponseEntity.ok().body(loginResponse);
    }

    public ResponseEntity<?> addMenu(MenuRequest menuRequest, Restaurant restaurant) {
        Menu menu = setNewMenu(menuRequest, restaurant);
        menuRepository.save(menu);
        return ResponseEntity.ok().body(menu);
    }

    public ResponseEntity<?> changeStatus(StatusMenu menuRequest, Restaurant restaurant) {
        Menu menu = menuRepository.findByMenuNameAndRestaurant(menuRequest.getMenuName(),restaurant);
        if(menu==null){
            throw new ValidationException("menu not found");
        }
        menu.setStatus(menuRequest.isStatus());
        return ResponseEntity.ok().body(menu);
    }

    public ResponseEntity<?> getMenu(Restaurant restaurant, String categoryName) {
        List<Menu> menu;
        if (categoryName != null) {
            Category category = categoryRepository.findByCategoryNameAndRestaurant(categoryName, restaurant);
            if (category == null) {
                throw new ValidationException("category doesn't Exist");
            }
            menu = menuRepository.findByRestaurantIdAndCategory(restaurant.getId(), category);
        } else {
            menu = menuRepository.findByRestaurantId(restaurant.getId());
        }
        return ResponseEntity.ok().body(menu);
    }

    public ResponseEntity<?> getCategory(Restaurant restaurant) {
        List<Category> categories = categoryRepository.findByRestaurant(restaurant);
        return ResponseEntity.ok().body(categories);
    }

    public ResponseEntity<?> addCategory(Restaurant restaurant, String categoryName) {
        Category category = categoryRepository.findByCategoryNameAndRestaurant(categoryName, restaurant);
        Category newCategory;
        if (category != null) {
            throw new ValidationException("category Already Exist");
        } else {
            newCategory = Category.builder().categoryName(categoryName).restaurant(restaurant).build();
            categoryRepository.save(newCategory);
        }
        return ResponseEntity.ok().body(newCategory);
    }

    public ResponseEntity<?> getOrder(Restaurant restaurant, String customerId) {
        List<OrderBos> orderBos;
        if (customerId == null) {
            log.info("customerId==null");
            orderBos = orderBosRepository.findByRestaurant(restaurant).orElseThrow(() -> new ValidationException("Order Not Found"));
            for (OrderBos item : orderBos) {
                for (OrderDetail detail : item.getOrderDetails()) {
                    detail.setName(detail.getMenu().getMenuName());
                    orderDetailRepository.save(detail);
                }
            }
        } else {
            log.info(customerId);
            orderBos = orderBosRepository.findByRestaurantAndUserId(restaurant, customerId).orElseThrow(() -> new ValidationException("Order Not Found"));
        }
        return ResponseEntity.ok().body(orderBos);
    }

    public ResponseEntity<?> getOrderFinished(Restaurant restaurant, String customerId) {
        List<OrderBos> orderBos;
        List<String> orderStatus = new ArrayList<>();
        orderStatus.add(OrderEnum.FINISHED_ORDER.getDescription());
        orderStatus.add(OrderEnum.CANCELED_ORDER.getDescription());
        if (customerId == null) {
            orderBos = orderBosRepository.findByRestaurantAndOrderStatusIn(restaurant, orderStatus);
        } else {
            log.info(customerId);
            orderBos = orderBosRepository.findByRestaurantAndUserIdAndOrderStatusIn(restaurant, customerId, orderStatus);
        }
        return ResponseEntity.ok().body(orderBos);
    }

    public ResponseEntity<?> getOrderOngoing(Restaurant restaurant, String customerId) {
        List<OrderBos> orderBos;
        List<String> orderStatus = new ArrayList<>();
        orderStatus.add(OrderEnum.ON_ORDER.getDescription());
        if (customerId == null) {
            orderBos = orderBosRepository.findByRestaurantAndOrderStatusIn(restaurant, orderStatus);
        } else {
            log.info(customerId);
            orderBos = orderBosRepository.findByRestaurantAndUserIdAndOrderStatusIn(restaurant, customerId, orderStatus);
        }
        return ResponseEntity.ok().body(orderBos);
    }

    public ResponseEntity<?> createOrder(Restaurant restaurant, OrderRequest request) {
        String userId = request.getUserId() == null ? String.valueOf(restaurant.getRestaurantId()) : request.getUserId();
        OrderBos orderBos = OrderBos.builder()
                .tableNumber(request.getTableNumber())
                .orderId(UUID.randomUUID())
                .userId(userId)
                .restaurant(restaurant)
                .createdAt(ZonedDateTime.now())
                .orderDetails(request.getOrderDetails())
                .build();
        long countPrice = 0;
        for (OrderDetail item : request.getOrderDetails()) {
            Menu menu = menuRepository.findByMenuNameAndRestaurant(item.getName(), restaurant);
            if (menu == null) {
                throw new ValidationException("Invalid Menu");
            }
            Long total;
            total = (item.getQty() * menu.getPrice());
            item.setAmount(total);
            item.setOrderDetailid(String.valueOf(UUID.randomUUID()));
            item.setMenu(menu);
            item.setOrderBos(orderBos);
            countPrice += total;
        }
        orderBos.setTotalPrice(countPrice);
        orderBosRepository.save(orderBos);
        return ResponseEntity.ok().body(orderBos);
    }

    public ResponseEntity<?> updateOrder(Restaurant restaurant, UpdateRequest request) {
        OrderBos orderBos = orderBosRepository.findByOrderIdAndRestaurant(UUID.fromString(request.getOrderId()), restaurant).orElseThrow(() -> new ValidationException("order not found"));
        List<OrderDetail> updatedOrderDetail = new ArrayList<>();
        long countPrice = 0;
        for (OrderDetail item : request.getOrderDetails()) {
            log.info(item.getOrderDetailid());
            if (item.getOrderDetailid() != null) {
                String status = StatusEnum.getEnumDesc(item.getStatus());
                Menu menu = menuRepository.findByMenuNameAndRestaurant(item.getName(), restaurant);
                if (menu == null) {
                    throw new ValidationException("Invalid Menu");
                }
                OrderDetail orderDetail = orderDetailRepository.findByOrderDetailidAndOrderBos(item.getOrderDetailid(), orderBos);
                if (orderDetail == null) {
                    throw new ValidationException("Invalid Order Id");
                }
                Long total;
                orderDetail.setQty(item.getQty());
                total = (item.getQty() * menu.getPrice());
                countPrice += total;
                orderDetail.setMenu(menu);
                orderDetail.setAmount(total);
                orderDetail.setDescription(item.getDescription());
                orderDetail.setStatus(status);
                updatedOrderDetail.add(orderDetail);
            } else {
                Menu menu = menuRepository.findByMenuNameAndRestaurant(item.getName(), restaurant);
                if (menu == null) {
                    throw new ValidationException("Invalid Menu");
                }
                Long total;
                total = (item.getQty() * menu.getPrice());
                countPrice += total;
                String status = StatusEnum.getEnumDesc(item.getStatus());
                OrderDetail newOrderDetail = OrderDetail.builder()
                        .orderDetailid(String.valueOf(UUID.randomUUID()))
                        .orderBos(orderBos)
                        .name(item.getName())
                        .amount(total)
                        .qty(item.getQty())
                        .status(status != null ? status : "0")
                        .menu(menu)
                        .build();
                if (item.getDescription() != null) {
                    newOrderDetail.setDescription(item.getDescription());
                }
                updatedOrderDetail.add(newOrderDetail);
            }
        }
        orderBos.setTableNumber(request.getTableNumber());
        orderBos.setTotalPrice(countPrice);
        orderBos.setOrderDetails(updatedOrderDetail);
        orderBosRepository.save(orderBos);
        return ResponseEntity.ok().body(orderBos);
    }

    public ResponseEntity<?> orderDetailStatus(Restaurant restaurant, UpdateStatusOrderDetailRequest orderRequest) {
        OrderBos orderBos = orderBosRepository.findByRestaurantAndOrderId(restaurant, orderRequest.getOrderId()).orElseThrow(() -> new ValidationException("no order found"));
        OrderDetail updatedOrderDetail = null;
        int orderDone = 0;
        if (!orderRequest.getStatusCode().equalsIgnoreCase(StatusEnum.ORDER_RECEIVE.getCode()) &&
                !orderRequest.getStatusCode().equalsIgnoreCase(StatusEnum.COOKING.getCode()) &&
                !orderRequest.getStatusCode().equalsIgnoreCase(StatusEnum.SERVE.getCode()) &&
                !orderRequest.getStatusCode().equalsIgnoreCase(StatusEnum.PREPARE.getCode())) {
            throw new ValidationException("status is only between 01-04");
        }
        for (OrderDetail item : orderBos.getOrderDetails()) {
            if (item.getOrderDetailid().equals(orderRequest.getOrderDetailId())) {
                item.setStatus(StatusEnum.getEnumDesc(orderRequest.getStatusCode()));
                updatedOrderDetail = item;
            }
            if (item.getStatus().equals(StatusEnum.SERVE.getDescription())) {
                orderDone += 1;
            }
        }
        orderBos.setOrderDone(orderDone);
        if (orderDone == orderBos.getOrderDetails().size()) {
            orderBos.setOrderStatus(OrderEnum.FINISHED_ORDER.getDescription());
        }

        if (updatedOrderDetail == null) {
            throw new ValidationException("order_detail_id not found");
        }
        orderBosRepository.save(orderBos);
        return ResponseEntity.ok().body(updatedOrderDetail);
    }

    public ResponseEntity<?> orderStatus(Restaurant restaurant, UpdateStatusOrderRequest orderRequest) {
        OrderBos orderBos = orderBosRepository.findByRestaurantAndOrderId(restaurant, orderRequest.getOrderId()).orElseThrow(() -> new ValidationException("no order found"));
        String desc = OrderEnum.getEnumDesc(orderRequest.getStatusCode());
        if(desc.equalsIgnoreCase("Order Cancelled")){
            for (OrderDetail item : orderBos.getOrderDetails()){
                if(item.getStatus().equalsIgnoreCase("Order Has been Serve")||item.getStatus().equalsIgnoreCase("Order Has been Cooking")){
                    throw new ValidationException("this order can'be cancelled because one of the menu already cook/finish");
                }
            }
        }
        orderBos.setOrderStatus(desc);
        orderBosRepository.save(orderBos);
        return ResponseEntity.ok().body(orderBos);
    }

    public ResponseEntity<?> reportOrder(Restaurant restaurant, ReportOrderRequest orderRequest) {
        String startDateString = orderRequest.getDate();
        int unit = orderRequest.getUnit();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date startDate;
        try {
            startDate = dateFormat.parse(startDateString);
        } catch (ParseException e) {
            return ResponseEntity.badRequest().body("Invalid date format");
        }

        Instant startInstant = startDate.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        ZonedDateTime startDateTime = ZonedDateTime.ofInstant(startInstant, zone);

        ZonedDateTime endDateTime = startDateTime.plusDays(unit);

        List<OrderBos> orders = orderBosRepository.findByRestaurantAndDateBetween(restaurant, startDateTime, endDateTime);
        List<OrderBos> shownOrders = new ArrayList<>();
        for (OrderBos order : orders) {
            if (order.getOrderStatus().equalsIgnoreCase(OrderEnum.FINISHED_ORDER.getDescription()) || order.getOrderStatus().equalsIgnoreCase(OrderEnum.CANCELED_ORDER.getDescription()))
            {
                shownOrders.add(order);
            }
        }
        if(shownOrders==null){
            return ResponseEntity.ok().body("No Data");
        }
        return ResponseEntity.ok().body(shownOrders);
    }


    public ResponseEntity<?> fullProfile(Restaurant restaurant, FullProfile profile) {
        FullProfile searchProfile = fullProfileRepository.findByRestaurant(restaurant);
        if(searchProfile==null){
            profile.setRestaurant(restaurant);
            profile.setPhoneNumber(restaurant.getPhoneNumber());
            restaurant.setFullProfile(profile);
            fullProfileRepository.save(profile);
            return ResponseEntity.ok().body(profile);
        }else{
            searchProfile.setMotto(profile.getMotto());
            searchProfile.setMottoDetail(profile.getMottoDetail());
            searchProfile.setLocation(profile.getLocation());
            searchProfile.setInstagramUrl(profile.getInstagramUrl());
            searchProfile.setFacebookUrl(profile.getFacebookUrl());
            searchProfile.setOpenDay(profile.getOpenDay());
            searchProfile.setOpenTime(profile.getOpenTime());
            searchProfile.setOfficialEmail(profile.getOfficialEmail());
            searchProfile.setRestaurantStory(profile.getRestaurantStory());
            searchProfile.setWhatsappNumber(profile.getWhatsappNumber());
            fullProfileRepository.save(searchProfile);
            return ResponseEntity.ok().body(searchProfile);
        }
    }

    public ResponseEntity<?> getProfile(Restaurant restaurant) {
        FullProfile profile = fullProfileRepository.findByRestaurant(restaurant);
        if(profile==null){
            throw new ValidationException("no profile");
        }
        return ResponseEntity.ok().body(profile);
    }

    public ResponseEntity<?> getData(Restaurant restaurant) {
        Restaurant restaurant1 = restaurantRepository.findByRestaurantId(restaurant.getRestaurantId());
        return ResponseEntity.ok().body(restaurant1.getRestaurant_name());
    }

    private Menu setNewMenu(MenuRequest menu, Restaurant restaurant) {
        if (menu.getId() == null) {
            Category category = categoryRepository.findByCategoryNameAndRestaurant(menu.getCategory(), restaurant);
            if (category == null) {
                throw new ValidationException("Category doesn't exist");
            }
            Menu checkMenuName = menuRepository.findByMenuNameAndRestaurant(menu.getMenuName(), restaurant);
            if (checkMenuName != null) {
                throw new ValidationException("menu_name already exist");
            }
            Menu newMenu = Menu.builder()
                    .menuName(menu.getMenuName())
                    .price(menu.getPrice())
                    .description(menu.getDescription())
                    .category(category)
                    .categoryName(category.getCategoryName())
                    .createdAt(ZonedDateTime.now())
                    .restaurant(restaurant)
                    .build();
            menuRepository.save(newMenu);
            return newMenu;
        } else {
            Menu updateMenu = menuRepository.findById(menu.getId()).orElseThrow(() -> new ValidationException("Menu Not Found"));
            Category category = categoryRepository.findByCategoryNameAndRestaurant(menu.getCategory(), restaurant);
            if (category == null) {
                throw new ValidationException("Category doesn't exist");
            }
            if(!menu.getMenuName().equals(updateMenu.getMenuName())){
                Menu checkMenuName = menuRepository.findByMenuNameAndRestaurant(menu.getMenuName(), restaurant);
                if (checkMenuName != null) {
                    throw new ValidationException("menu_name already exist");
                }
                updateMenu.setMenuName(menu.getMenuName());
            }
            updateMenu.setCategory(category);
            updateMenu.setCategoryName(category.getCategoryName());
            updateMenu.setPrice(menu.getPrice());
            updateMenu.setDescription(menu.getDescription());
            menuRepository.save(updateMenu);
            return updateMenu;
        }
    }

    private static String getEncryptedPassword(String password) {
        BCryptPasswordEncoder crypt = new BCryptPasswordEncoder();
        String encryptedPassword = crypt.encode(password);
        return encryptedPassword;
    }

    private static boolean checkPassword(String password, String decodePassword) {
        BCryptPasswordEncoder crypt = new BCryptPasswordEncoder();
        boolean encryptedPassword = crypt.matches(password, decodePassword);
        return encryptedPassword;
    }

}
