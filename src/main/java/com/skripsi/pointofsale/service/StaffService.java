package com.skripsi.pointofsale.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skripsi.pointofsale.jpa.entity.Restaurant;
import com.skripsi.pointofsale.jpa.entity.Staff;
import com.skripsi.pointofsale.jpa.repository.RestaurantRepository;
import com.skripsi.pointofsale.exception.ValidationException;
import com.skripsi.pointofsale.jpa.repository.StaffRepository;
import com.skripsi.pointofsale.request.staff.LoginRequest;
import com.skripsi.pointofsale.request.staff.StaffRegisterRequest;
import com.skripsi.pointofsale.request.staff.UpdateRequest;
import com.skripsi.pointofsale.response.restaurant.SuccessResponse;
import com.skripsi.pointofsale.response.staff.RegisterResponse;
import com.skripsi.pointofsale.security.JWTGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class StaffService {
    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    StaffRepository staffRepository;

    @Autowired
    RestaurantRepository restaurantRepository;

    @Autowired
    private JWTGenerator jwtGenerator;
    public ResponseEntity<RegisterResponse> register(StaffRegisterRequest staffRegisterRequest, Restaurant restaurant) throws JsonProcessingException {

        String staffId = staffRegisterRequest.getName().toLowerCase()+"-"+ UUID.randomUUID();
        String encryptedPassword = getEncryptedPassword(staffRegisterRequest.getPassword());
        Staff checkEmail =staffRepository.findByEmail(staffRegisterRequest.getEmail());
        if(checkEmail!=null){
            throw new ValidationException("email already register, please use different email");
        }
        if(staffRegisterRequest.getRole()==null){
                if(staffRegisterRequest.getRole().equalsIgnoreCase("Cashier") || staffRegisterRequest.getRole().equalsIgnoreCase("Koki")){
                    throw new ValidationException("role only cashier or koki");
                }
                staffRegisterRequest.setRole(staffRegisterRequest.getRole());
        }
        if(!staffRegisterRequest.getRole().equalsIgnoreCase("cashier") && !staffRegisterRequest.getRole().equalsIgnoreCase("manager")){
            throw new ValidationException("role only cashier and manager");
        }

        Staff staff = Staff.builder().
                createdAt(ZonedDateTime.now()).
                role(staffRegisterRequest.getRole()).
                staffId(staffId).
                staffName(staffRegisterRequest.getName()).
                email(staffRegisterRequest.getEmail()).
                password(encryptedPassword).
                phoneNumber(staffRegisterRequest.getPhoneNumber())
                .restaurant(restaurant).build();
        staffRepository.save(staff);
        RegisterResponse registerResponse = RegisterResponse.builder().staffId(staffId).role(staff.getRole()).build();
        return ResponseEntity.ok().body(registerResponse);
    }

    public ResponseEntity<?> login(LoginRequest loginRequest){
        Staff staff = staffRepository.findByEmail(loginRequest.getEmail());
        if(staff==null){
            throw new ValidationException("wrong restaurant_id");
        }
        if(!checkPassword(loginRequest.getPassword(),staff.getPassword())){
            throw new ValidationException("wrong password");
        }
        Date expirationDate = jwtGenerator.getExpirationFromJWT(staff.getRestaurant().getTokenInfo());
        jwtGenerator.validateToken(staff.getRestaurant().getTokenInfo());
        SuccessResponse successResponse = SuccessResponse.builder().restaurantId(staff.getRestaurant().getRestaurantId()).restaurantName(staff.getRestaurant().getRestaurant_name()).token(staff.getRestaurant().getTokenInfo()).staffName(staff.getStaffName()).staffId(staff.getStaffId()).role(staff.getRole()).responseMessage("SUCCESS").status(HttpStatus.OK.value()).build();
        return ResponseEntity.ok().body(successResponse);
    }
    public ResponseEntity<?> getUsers(Restaurant restaurant, String id){
        List<Staff> listStaff= staffRepository.findByRestaurantId(restaurant.getId());
        if(listStaff.isEmpty()){
            throw new ValidationException("No Staff");
        }
        if(id!=null){
            for (Staff item: listStaff) {
                if(item.getStaffId().equalsIgnoreCase(id)){
                    return ResponseEntity.ok().body(item);
                }
            }
        }
        if(id==null){
            return ResponseEntity.ok().body(listStaff);
        }
        return ResponseEntity.ok().body("No Staff");

    }

    public ResponseEntity<?> updateUsers(UpdateRequest updateRequest){
        Staff staff  = staffRepository.findByStaffId(updateRequest.getStaffId());
        if(staff == null){
            throw new ValidationException("Invalid Staff id");
        }
        updateDataUser(updateRequest, staff);
        return ResponseEntity.ok().body(staff);
    }

    private void updateDataUser(UpdateRequest updateRequest, Staff staff) {
        if(updateRequest.getName()!=null){
            staff.setStaffName(updateRequest.getName());
        }
        if(updateRequest.getEmail()!=null){
            staff.setEmail(updateRequest.getEmail());
        }
        if(updateRequest.getPhoneNumber()!=null){
            staff.setPhoneNumber(updateRequest.getPhoneNumber());
        }
        if(updateRequest.getRole()!=null){
            staff.setRole(updateRequest.getRole());
        }
        staff.setUpdatedAt(ZonedDateTime.now());
        staffRepository.save(staff);
    }

    private List<Staff> createStaffList(List<Staff> staff) {
        List<Staff> itemEntities = new ArrayList<>();
        for (Staff item : staff) {
            itemEntities.add(Staff.builder().staffId(item.getStaffId()).createdAt(item.getCreatedAt()).staffName(item.getStaffName()).email(item.getEmail()).phoneNumber(item.getPhoneNumber()).build());
        }

        return itemEntities;
    }
    private static String getEncryptedPassword(String password) {
        BCryptPasswordEncoder crypt = new BCryptPasswordEncoder();
        String encryptedPassword = crypt.encode(password);
        log.info("pass{} | encrpt{}",password,encryptedPassword);
        return encryptedPassword;
    }

    private static boolean checkPassword(String password,String decodePassword) {
        BCryptPasswordEncoder crypt = new BCryptPasswordEncoder();
        boolean encryptedPassword = crypt.matches(password,decodePassword);
        return encryptedPassword;
    }
}
