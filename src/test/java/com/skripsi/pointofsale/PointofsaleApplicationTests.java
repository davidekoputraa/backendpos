package com.skripsi.pointofsale;

import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = PointofsaleApplication.class)
@DataJpaTest
class PointofsaleApplicationTests {

//    @Test
//    void contextLoads() {
//    }

}
